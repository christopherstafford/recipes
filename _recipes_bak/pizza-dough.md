---
layout: recipe
title: "Pizza Dough"
tags: pizza, dough, bake

directions:
- Mix dry ingredients in large bowl
- Add water
- Combine well until smooth
- Rest 45 minutes uncovered
- Kneed until smooth
- Rest 6 hours covered
- Shape into 5 ~350g balls
- Cover and rest on baking sheet for 1 hour
- Place in fridge for 30 minutes before using

components:
- 1kg Bread flour
- 700g Water (90-95 degress)
- 20g Fine Seasalt
- 2g Instant dried yeast

---

Stephens recipe
