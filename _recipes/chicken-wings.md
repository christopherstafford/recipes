---
layout: recipe
title: "Buffalo Chicken Wings"
tags: gameday, bar-food, chicken, wings

directions:
- Setup grill for indirect cooking without heat deflectos
- Preheat grill to 325-350
- Seperate wingettes from drumettes
- Pat dry wings
- Brush wings with olive oil
- Season wings
- Place wings on indirect side of grill
- Let cook for 30 minutes
- Flip wings
- Check temperature at 35 minutes
- Remove at 170 degrees+
- Toss in sauce of choice

components:
- Chicken wings
- Rub of choice
- Sauce of choice
- Olive oil

---

A great game day treat
